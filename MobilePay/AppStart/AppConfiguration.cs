﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MobilePay.DataAccess.Readers;
using MobilePay.Domain.Interfaces.BusinessLogic;
using MobilePay.Domain.Interfaces.Readers;
using MobilePay.Domain.Interfaces.Services;
using MobilePay.Services.BusinessLogic;
using MobilePay.Services.Services;
using System;
using System.IO;
using System.Text;

namespace MobilePay.ConsoleApp.AppStart
{
    public static class AppConfiguration
    {
        public static IConfigurationRoot SetUpAppConfiguration()
        {
            Console.OutputEncoding = Encoding.UTF8;

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            
            return builder.Build();
        }

        public static IServiceProvider SetUpServiceProvider()
        {
            var serviceCollection = new ServiceCollection()
                .AddSingleton<ITransactionReader, TransactionReader>(x => new TransactionReader(AppSettings.GetDataFilePath()))
                .AddSingleton<IFeeCalculationService, FeeCalculationService>()
                .AddSingleton<IFeeEngine, FeeEngine>(x => new FeeEngine(AppSettings.GetFeeCalculationRuleCollection(), new TransactionTracker()));

            return serviceCollection.BuildServiceProvider();
        }
    }
}
