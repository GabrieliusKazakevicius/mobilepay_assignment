﻿using MobilePay.Services.Models.FeeEngine;
using System.Collections.Generic;

namespace MobilePay.ConsoleApp
{
    public static class AppSettings
    {
        public static string GetDataFilePath() => @"..\..\..\..\transactions.txt";

        public static FeeCalculationRuleCollection GetFeeCalculationRuleCollection()
            => new FeeCalculationRuleCollection
            {
                FeeList = new List<BaseFeeCalculationRule>
                {
                    new TransactionPercentageFee()
                },
                DiscountList = new List<BaseFeeCalculationRule>
                {
                    new CircleKDiscount(),
                    new TeliaDiscount()
                },
                MonthlyFeeList = new List<BaseFeeCalculationRule>
                {
                    new FirstMonthlyTransactionFee()
                }
            };
    }
}
