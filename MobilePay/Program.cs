﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MobilePay.ConsoleApp.AppStart;
using MobilePay.Domain.Dtos;
using MobilePay.Domain.Interfaces.Services;
using System;

namespace MobilePay.ConsoleApp
{
    class Program
    {
        public static IConfigurationRoot Configuration;
        public static IServiceProvider ServiceProvider;

        public static void Main(string[] args)
        {
            try
            {
                RunAppSetup();
                var feeCalculationService = ServiceProvider.GetService<IFeeCalculationService>();

                feeCalculationService.CalculateFees(WriteResultAction);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
        }

        private static void RunAppSetup()
        {
            Configuration = AppConfiguration.SetUpAppConfiguration();
            ServiceProvider = AppConfiguration.SetUpServiceProvider();
        }

        private static void WriteResultAction(FeeResultDto feeResultDto)
        {
            Console.WriteLine(feeResultDto?.ToString());
        }

        private static void HandleException(Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

        
    }
}
