﻿using MobilePay.Domain.Interfaces.Readers;
using MobilePay.Domain.Models;
using System;
using System.IO;
using System.Linq;

namespace MobilePay.DataAccess.Readers
{
    public class TransactionReader : ITransactionReader
    {
        private readonly StreamReader _streamReader;

        public TransactionReader(string dataFilePath)
        {
            _streamReader = new StreamReader(dataFilePath);
        }

        /// <summary>
        /// Read single transaction from source file
        /// </summary>
        /// <param name="transaction">Parsed transaction data, if data is invalid or an error occurs, this param is null</param>
        /// <returns>Returns bool value indicating that stream still has unread data</returns>
        public bool Read(out Transaction transaction)
        {
            if (!_streamReader.EndOfStream)
            {
                var dataParams = ParseDataParameters(_streamReader);
                transaction = MapDataParametersToTransaction(dataParams);
                
                return true;
            }
            else
            {
                transaction = null;
                return false;
            }
        }

        private string[] ParseDataParameters(StreamReader streamReader)
        {
            var dataString = streamReader.ReadLine();
            return dataString.Split(' ', StringSplitOptions.RemoveEmptyEntries);
        }

        private Transaction MapDataParametersToTransaction(string[] dataParams)
        {
            if (!dataParams.Any())
                return null;

            return new Transaction
            {
                Date = DateTime.Parse(dataParams[0]),
                MerchantName = dataParams[1],
                Amount = float.Parse(dataParams[2])
            };
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _streamReader.Dispose();
            }
        }
    }
}
