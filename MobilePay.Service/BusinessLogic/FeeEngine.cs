﻿using MobilePay.Domain.Interfaces.BusinessLogic;
using MobilePay.Domain.Models;
using MobilePay.Services.Models.FeeEngine;
using System.Collections.Generic;

namespace MobilePay.Services.BusinessLogic
{
    public class FeeEngine : IFeeEngine
    {
        private readonly ITransactionTracker _transactionTracker;
        private readonly FeeCalculationRuleCollection _calculationRules;

        public FeeEngine(FeeCalculationRuleCollection calculationRuleCollection, ITransactionTracker transactionTracker)
        {
            _calculationRules = calculationRuleCollection;
            _transactionTracker = transactionTracker;
        }
        
        public float CalculateFee(Transaction transaction)
        {
            var feeCalculationModel = new FeeCalculationModel
            {
                TotalFee = 0f,
                Transaction = transaction
            };

            ProcessFeeCalculationRules(feeCalculationModel, _calculationRules.FeeList);
            ProcessFeeCalculationRules(feeCalculationModel, _calculationRules.DiscountList);

            if (_transactionTracker.IsFirstTransactionOfMonth(transaction))
            {
                ProcessFeeCalculationRules(feeCalculationModel, _calculationRules.MonthlyFeeList);
            }

            return feeCalculationModel.TotalFee;
        }

        private void ProcessFeeCalculationRules(FeeCalculationModel feeCalculationModel, IEnumerable<BaseFeeCalculationRule> feeRuleList)
        {
            foreach(var fee in feeRuleList)
            {
                fee.Apply(feeCalculationModel);
            }
        }
    }
}
