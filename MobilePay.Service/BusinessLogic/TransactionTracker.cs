﻿using MobilePay.Domain.Interfaces.BusinessLogic;
using MobilePay.Domain.Models;
using MobilePay.Services.Extensions;
using System;
using System.Collections.Generic;

namespace MobilePay.Services.BusinessLogic
{
    public class TransactionTracker : ITransactionTracker
    {
        private readonly Dictionary<string, DateTime> _latestTransactionList;
        private DateTime _lastTransactionDate;

        public TransactionTracker()
        {
            _latestTransactionList = new Dictionary<string, DateTime>();
        }

        public bool IsFirstTransactionOfMonth(Transaction transaction)
        {
            var isFirst = false;
            if (!_latestTransactionList.ContainsKey(transaction.MerchantName))
            {
                isFirst = true;
            }
            else
            {
                if (!_latestTransactionList[transaction.MerchantName].IsSameMonth(transaction.Date))
                {
                    isFirst = true;
                }
                if (!_lastTransactionDate.IsSameMonth(transaction.Date))
                {
                    StartNewMonth();
                }
            }

            _lastTransactionDate = transaction.Date;
            UpdateLastTransactionList(transaction);
            return isFirst;
        }

        private void UpdateLastTransactionList(Transaction transaction)
        {
            if (!_latestTransactionList.ContainsKey(transaction.MerchantName))
            {
                _latestTransactionList.Add(transaction.MerchantName, transaction.Date);
            }
            else
            {
                _latestTransactionList[transaction.MerchantName] = transaction.Date;
            }
        }

        private void StartNewMonth()
        {
            _latestTransactionList.Clear();
        }
    }
}
