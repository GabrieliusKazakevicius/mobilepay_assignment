﻿using MobilePay.Domain.Dtos;
using MobilePay.Domain.Interfaces.BusinessLogic;
using MobilePay.Domain.Interfaces.Readers;
using MobilePay.Domain.Interfaces.Services;
using MobilePay.Domain.Models;
using System;

namespace MobilePay.Services.Services
{
    public class FeeCalculationService : IFeeCalculationService
    {
        private readonly ITransactionReader _transactionReader;
        private readonly IFeeEngine _feeEngine;

        public FeeCalculationService(ITransactionReader transactionReader, IFeeEngine feeEngine)
        {
            _transactionReader = transactionReader;
            _feeEngine = feeEngine;
        }

        public void CalculateFees(Action<FeeResultDto> writeResultAction)
        {
            while (_transactionReader.Read(out Transaction transaction))
            {
                if (transaction == null)
                {
                    writeResultAction(null);
                    continue;
                }
                else
                {
                    var feeModel = CalculateFeeForTransaction(transaction);
                    writeResultAction(feeModel);
                }                
            }
        }
        
        private FeeResultDto CalculateFeeForTransaction(Transaction transaction)
        {
            var totalFee = _feeEngine.CalculateFee(transaction);

            var feeResult = new FeeResultDto
            {
                MerchantName = transaction.MerchantName,
                Date = transaction.Date,
                Fee = totalFee
            };

            return feeResult;
        }
    }
}
