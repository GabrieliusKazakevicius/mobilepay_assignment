﻿namespace MobilePay.Services.Models.FeeEngine
{
    public class FirstMonthlyTransactionFee : BaseFeeCalculationRule
    {
        public FirstMonthlyTransactionFee() : base()
        {
            _flatValue = 29f;
        }

        public override void Apply(FeeCalculationModel calculationModel)
        {
            calculationModel.TotalFee += _flatValue;
        }
    }
}
