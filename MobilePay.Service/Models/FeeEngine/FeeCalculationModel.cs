﻿using MobilePay.Domain.Models;

namespace MobilePay.Services.Models.FeeEngine
{
    public class FeeCalculationModel
    {
        public float TotalFee { get; set; }
        public Transaction Transaction { get; set; }
    }
}
