﻿using System.Collections.Generic;

namespace MobilePay.Services.Models.FeeEngine
{
    public class FeeCalculationRuleCollection
    {
        public IEnumerable<BaseFeeCalculationRule> FeeList { get; set; }
        public IEnumerable<BaseFeeCalculationRule> DiscountList { get; set; }
        public IEnumerable<BaseFeeCalculationRule> MonthlyFeeList { get; set; }
    }
}
