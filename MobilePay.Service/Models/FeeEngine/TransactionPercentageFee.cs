﻿namespace MobilePay.Services.Models.FeeEngine
{
    public class TransactionPercentageFee : BaseFeeCalculationRule
    {
        public TransactionPercentageFee() : base()
        {
            _percentageValue = 0.01f;
        }

        public override void Apply(FeeCalculationModel calculationModel)
        {
            calculationModel.TotalFee += (calculationModel.Transaction.Amount * _percentageValue);
        }
    }
}
