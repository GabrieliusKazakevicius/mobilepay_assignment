﻿namespace MobilePay.Services.Models.FeeEngine
{
    public class CircleKDiscount : BaseFeeCalculationRule
    {
        public CircleKDiscount() : base()
        {
            _percentageValue = 0.2f;
        }

        public override void Apply(FeeCalculationModel calculationModel)
        {
            var transaction = calculationModel.Transaction;
            if (transaction.MerchantName.ToLowerInvariant().Equals("circle_k"))
            {
                calculationModel.TotalFee -= calculationModel.TotalFee * _percentageValue;
            }
        }
    }
}
