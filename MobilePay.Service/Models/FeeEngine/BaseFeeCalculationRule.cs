﻿namespace MobilePay.Services.Models.FeeEngine
{
    public abstract class BaseFeeCalculationRule
    {
        protected float _flatValue;
        protected float _percentageValue;

        public BaseFeeCalculationRule()
        {
            if (_percentageValue.Equals(default))
                _flatValue = 0f;
            if (_percentageValue.Equals(default))
                _percentageValue = 0f;                
        }

        public abstract void Apply(FeeCalculationModel transaction);
    }
}
