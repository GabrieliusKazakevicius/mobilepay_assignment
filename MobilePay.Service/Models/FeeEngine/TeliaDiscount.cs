﻿namespace MobilePay.Services.Models.FeeEngine
{
    public class TeliaDiscount : BaseFeeCalculationRule
    {
        public TeliaDiscount() : base()
        {
            _percentageValue = 0.1f;
        }

        public override void Apply(FeeCalculationModel calculationModel)
        {
            var transaction = calculationModel.Transaction;
            if (transaction.MerchantName.ToLowerInvariant().Equals("telia"))
            {
                calculationModel.TotalFee -= calculationModel.TotalFee * _percentageValue;
            }
        }
    }
}
