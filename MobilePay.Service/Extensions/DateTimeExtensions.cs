﻿using System;

namespace MobilePay.Services.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsSameMonth(this DateTime date, DateTime dateToCompare)
        {
            var monthsApart = 12 * (date.Year - dateToCompare.Year) + date.Month - dateToCompare.Month;
            return Math.Abs(monthsApart).Equals(0);
        }
    }
}
