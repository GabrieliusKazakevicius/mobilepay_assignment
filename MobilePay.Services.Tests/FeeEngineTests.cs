﻿using MobilePay.Services.BusinessLogic;
using MobilePay.Services.Models.FeeEngine;
using MobilePay.Services.Tests.TestDataProviders;
using System.Collections.Generic;
using Xunit;

namespace MobilePay.Services.Tests
{
    public class FeeEngineTests
    {
        private float _testTransactionAmount = 100;
        private float _testTransactionInitialFee =  1.0f;
        private float _testTransactionTeliaDiscountFee = 0.9f;
        private float _testTransactionCircleKDiscountFee = 0.8f;
        private float _testFirstMonthTransactionFee = 29f;

        [Fact]
        public void TransactionPercentageFee_ReturnsCorrectFee()
        {
            //Arrange
            var transaction = TransactionProvider.FirstTransaction("BITE");
            transaction.Amount = _testTransactionAmount;
            var feeEngine = new FeeEngine(
                new FeeCalculationRuleCollection{
                    FeeList = new List<BaseFeeCalculationRule> { new TransactionPercentageFee() },
                    DiscountList = new List<BaseFeeCalculationRule>(),
                    MonthlyFeeList = new List<BaseFeeCalculationRule>()
                },
                new TransactionTracker()
            );

            //Act
            var fee = feeEngine.CalculateFee(transaction);


            //Assert
            Assert.Equal(_testTransactionInitialFee, fee);
        }


        [Fact]
        public void TeliaGetsTeliaDiscount()
        {
            //Arrange
            var transaction = TransactionProvider.FirstTransaction("TELIA");
            transaction.Amount = _testTransactionAmount;

            var feeEngine = new FeeEngine(
                new FeeCalculationRuleCollection
                {
                    FeeList = new List<BaseFeeCalculationRule> { new TransactionPercentageFee() },
                    DiscountList = new List<BaseFeeCalculationRule> { new TeliaDiscount()},
                    MonthlyFeeList = new List<BaseFeeCalculationRule>()
                },
                new TransactionTracker()
            );

            //Act
            var fee = feeEngine.CalculateFee(transaction);


            //Assert
            Assert.Equal(_testTransactionTeliaDiscountFee, fee);
        }

        [Fact]
        public void CircleKGetsCircleKDiscount()
        {
            //Arrange
            var transaction = TransactionProvider.FirstTransaction("CIRCLE_K");
            transaction.Amount = _testTransactionAmount;

            var feeEngine = new FeeEngine(
                new FeeCalculationRuleCollection
                {
                    FeeList = new List<BaseFeeCalculationRule> { new TransactionPercentageFee() },
                    DiscountList = new List<BaseFeeCalculationRule> { new CircleKDiscount() },
                    MonthlyFeeList = new List<BaseFeeCalculationRule>()
                },
                new TransactionTracker()
            );

            //Act
            var fee = feeEngine.CalculateFee(transaction);


            //Assert
            Assert.Equal(_testTransactionCircleKDiscountFee, fee);
        }

        [Fact]
        public void TeliaGetsDoesNotGetCircleKDiscount()
        {
            //Arrange
            var transaction = TransactionProvider.FirstTransaction("TELIA");
            transaction.Amount = _testTransactionAmount;

            var feeEngine = new FeeEngine(
                new FeeCalculationRuleCollection
                {
                    FeeList = new List<BaseFeeCalculationRule> { new TransactionPercentageFee() },
                    DiscountList = new List<BaseFeeCalculationRule> { new CircleKDiscount() },
                    MonthlyFeeList = new List<BaseFeeCalculationRule>()
                },
                new TransactionTracker()
            );

            //Act
            var fee = feeEngine.CalculateFee(transaction);


            //Assert
            Assert.Equal(_testTransactionInitialFee, fee);
        }

        [Fact]
        public void OnlyFirstMonthlyTransactionGetsMonthlyFee()
        {
            //Arrange
            var firstTransaction = TransactionProvider.FirstTransaction("TELIA");
            var seccondTransaction = TransactionProvider.SeccondTransaction("TELIA");
            firstTransaction.Amount = _testTransactionAmount;
            seccondTransaction.Amount = _testTransactionAmount;

            var feeEngine = new FeeEngine(
                new FeeCalculationRuleCollection
                {
                    FeeList = new List<BaseFeeCalculationRule>(),
                    DiscountList = new List<BaseFeeCalculationRule>(),
                    MonthlyFeeList = new List<BaseFeeCalculationRule> { new FirstMonthlyTransactionFee() }
                },
                new TransactionTracker()
            );

            //Act
            var firstMonthlyFee = feeEngine.CalculateFee(firstTransaction);
            var seccondMonthlyFee = feeEngine.CalculateFee(seccondTransaction);


            //Assert
            Assert.Equal(_testFirstMonthTransactionFee, firstMonthlyFee);
            Assert.Equal(0, seccondMonthlyFee);
        }
    }
}
