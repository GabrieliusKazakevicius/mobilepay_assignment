﻿using MobilePay.Domain.Models;
using System;

namespace MobilePay.Services.Tests.TestDataProviders
{
    public static class TransactionProvider
    {
        public static Transaction FirstTransaction(string merchName) => new Transaction { MerchantName = merchName, Amount = 110, Date = new DateTime(2019, 7, 27) };
        public static Transaction SeccondTransaction(string merchName) => new Transaction { MerchantName = merchName, Amount = 110, Date = new DateTime(2019, 7, 29) };
        public static Transaction NewMonthFirstTransaction(string merchName) => new Transaction { MerchantName = merchName, Amount = 110, Date = new DateTime(2019, 8, 1) };
        public static Transaction NewMonthSeccondTransaction(string merchName) => new Transaction { MerchantName = merchName, Amount = 110, Date = new DateTime(2019, 8, 3) };

    }
}
