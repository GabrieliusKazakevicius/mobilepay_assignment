﻿using MobilePay.Domain.Models;
using MobilePay.Services.BusinessLogic;
using MobilePay.Services.Tests.TestDataProviders;
using System;
using Xunit;

namespace MobilePay.Services.Tests
{
    public class TransactionTrackerTests
    {
        private TransactionTracker _transactionTracker;
        public TransactionTrackerTests()
        {
            _transactionTracker = new TransactionTracker();
        }

        [Fact]
        public void SingleMerchantTransaction_ReturnsTrue()
        {
            //Act
            var isFirstTransaction = _transactionTracker.IsFirstTransactionOfMonth(TransactionProvider.FirstTransaction("BITE"));

            //Assert
            Assert.True(isFirstTransaction);
        }
        [Fact]
        public void TwoSameMerchantTransactionsInSameMonth_ReturnFalse()
        {
            //Arrange
            var firstTransaction = new Transaction { MerchantName = "BITE", Amount = 110, Date = new DateTime(2019, 7, 27) };
            var seccondTransaction = new Transaction { MerchantName = "BITE", Amount = 110, Date = new DateTime(2019, 7, 29) };

            //Act
            _transactionTracker.IsFirstTransactionOfMonth(TransactionProvider.FirstTransaction("BITE"));
            var isFirstTransaction = _transactionTracker.IsFirstTransactionOfMonth(TransactionProvider.SeccondTransaction("BITE"));

            //Assert
            Assert.False(isFirstTransaction);
        }

        [Fact]
        public void TransactionInNewMonth_ReturnsTrue()
        {
            //Act
            _transactionTracker.IsFirstTransactionOfMonth(TransactionProvider.FirstTransaction("BITE"));
            _transactionTracker.IsFirstTransactionOfMonth(TransactionProvider.SeccondTransaction("BITE"));
            var isFirstTransaction = _transactionTracker.IsFirstTransactionOfMonth(TransactionProvider.NewMonthFirstTransaction("BITE"));

            //Assert
            Assert.True(isFirstTransaction);
        }

        [Fact]
        public void TransactionInNewMonth_ResetsAllPreviousTransactionData()
        {
            //Act
            _transactionTracker.IsFirstTransactionOfMonth(TransactionProvider.FirstTransaction("BITE"));
            _transactionTracker.IsFirstTransactionOfMonth(TransactionProvider.SeccondTransaction("BITE"));
            _transactionTracker.IsFirstTransactionOfMonth(TransactionProvider.NewMonthFirstTransaction("NOT_BITE"));
            var isFirstTransaction = _transactionTracker.IsFirstTransactionOfMonth(TransactionProvider.NewMonthSeccondTransaction("BITE"));

            //Assert
            Assert.True(isFirstTransaction);
        }
    }
}
