﻿using MobilePay.Domain.Models;
using System;

namespace MobilePay.Domain.Interfaces.Readers
{
    public interface ITransactionReader : IDisposable
    {
        bool Read(out Transaction transaction);
    }
}
