﻿using MobilePay.Domain.Dtos;
using System;

namespace MobilePay.Domain.Interfaces.Services
{
    public interface IFeeCalculationService
    {
        void CalculateFees(Action<FeeResultDto> writeResultAction);
    }
}
