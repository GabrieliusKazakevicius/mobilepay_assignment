﻿using MobilePay.Domain.Models;

namespace MobilePay.Domain.Interfaces.BusinessLogic
{
    public interface ITransactionTracker
    {
        bool IsFirstTransactionOfMonth(Transaction transaction);
    }
}
