﻿using MobilePay.Domain.Models;

namespace MobilePay.Domain.Interfaces.BusinessLogic
{
    public interface IFeeEngine
    {
        float CalculateFee(Transaction transaction);
    }
}
