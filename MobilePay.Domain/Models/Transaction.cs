﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobilePay.Domain.Models
{
    public class Transaction
    {
        public string MerchantName { get; set; }
        public DateTime Date { get; set; }
        public float Amount { get; set; }
    }
}
