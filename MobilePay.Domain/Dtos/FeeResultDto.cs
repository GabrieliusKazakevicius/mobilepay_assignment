﻿using System;

namespace MobilePay.Domain.Dtos
{
    public class FeeResultDto
    {
        public string MerchantName { get; set; }
        public DateTime Date { get; set; }
        public float Fee { get; set; }

        public override string ToString()
        {
            var date = Date.ToString("yyyy-MM-dd");
            var merchName = MerchantName;
            var fee = Fee.ToString("0.00");

            return $"{date} {merchName} {fee}";
        }
    }
}
